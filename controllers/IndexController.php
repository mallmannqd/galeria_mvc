<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 16:50
 */

namespace controllers;

use core\Controller;
use models\Fotos;

class IndexController extends Controller
{
    public function index()
    {
        $fotos = new Fotos();
        $dados['fotos'] = $fotos->getFotos();

        $this->loadTemplate('index/index', $dados);
    }

    public function add(): void
    {
        try{

            $fotos = new Fotos();
            $fotos->add();
            $this->flashMessage('success', 'Imagem enviada com sucesso');
            header('Location: ' . BASE_URL);

        }catch (\PDOException | \Exception $e){

            $this->flashMessage('danger', $e->getMessage());
            header('Location: ' . BASE_URL);

        }
    }

}