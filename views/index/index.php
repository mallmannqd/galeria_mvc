<h1>Bem vindo!</h1>
<fieldset>
    <legend>Adicionar uma Foto</legend>
    <?=$this->showMessage();?>

    <form method="post" action="<?=BASE_URL?>index/add" enctype="multipart/form-data">
        <label for="titulo">Titulo:</label>
        <input type="text" id="titulo" name="titulo" class="form-control">

        <label for="arquivo">Foto:</label>
        <input type="file" name="arquivo" id="arquivo">

        <button type="submit" class="btn btn-success">Enviar</button>
    </form>
</fieldset>
<br><br>
<?php foreach ($fotos as $foto) :?>
    <img src="<?=BASE_URL?>assets/images/<?=$foto->getUrl()?>" border="0" width="300"><br>
    <?=$foto->getTitulo()?>
    <hr>
<?php endforeach; ?>
