<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 19/04/18
 * Time: 08:43
 */

namespace models;


use core\Model;

class Fotos extends Model
{
    private $id;
    private $titulo;
    private $url;

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitulo(): String
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     */
    public function setTitulo($titulo): void
    {
        $this->titulo = $titulo;
    }

    /**
     * @return mixed
     */
    public function getUrl(): String
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    public function getFotos(): array
    {
        $sql = "SELECT * FROM fotos";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();

        $result = $stmt->fetchAll();

        $fotos = [];

        if (!empty($result)){
            foreach ($result as $r){
                $foto = new Fotos();
                $foto->setId($r['id']);
                $foto->setTitulo($r['titulo']);
                $foto->setUrl($r['url']);

                array_push($fotos, $foto);
            }
        }

        return $fotos;
    }

    public function add() : void
    {

        if (isset($_FILES['arquivo']) && !empty($_FILES['arquivo']['tmp_name'])) {

            $permitidos = [
                'image/jpeg',
                'image/jpg',
                'image/png'
            ];

            if (in_array($_FILES['arquivo']['type'], $permitidos)) {

                $nome = md5(time() . rand(0, 999)) . '.jpg';

                move_uploaded_file($_FILES['arquivo']['tmp_name'], FULL_PATH . 'assets/images/' . $nome);

                if (isset($_POST['titulo']) && !empty($_POST['titulo'])) {

                    $sql = "INSERT INTO fotos (titulo, url) VALUES (:titulo, :url)";
                    $stmt = $this->db->prepare($sql);

                    $stmt->bindValue(':titulo', $_POST['titulo']);
                    $stmt->bindValue(':url', $nome);

                    $stmt->execute();

                }

                throw new \Exception('Título necessário para envio');

            }

            throw new \Exception('Formato do arquivo inválido');

        }

        throw new \Exception('Favor enviar um arquivo');

    }
}